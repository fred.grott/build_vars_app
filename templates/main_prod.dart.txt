import 'package:flutter/material.dart';
import 'package:build_vars_app/envs.dart';
import 'app.dart';

void main() {
  BuildEnvironment.init(
      flavor: BuildFlavor.production, baseUrl: 'http://example.com', email:'fred.grott@gmailo.com');
  assert(env != null);
  runApp(MyApp());
}